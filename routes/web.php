<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test','FirebaseController@index');

Route::get('/teamregister', function(){
	return view('teamregistration');
});

Route::post('/teamregister', 'FirebaseController@registerTeam');

Route::get('/ranking', function(){
	return view('ranking');
});

Route::get('/station/{id}', 'FirebaseController@station');

Route::get('/progress/{id}/{key}', 'FirebaseController@progress');