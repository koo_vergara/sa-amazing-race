<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use DateTime;
use DateTimeZone;

class FirebaseController extends Controller
{
    public function index()
    {
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/sa-amazing-race-firebase-adminsdk-fesai-dcd034b635.json');
		$firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://sa-amazing-race.firebaseio.com/')->create();
		$database = $firebase->getDatabase();
		$newPost = $database->getReference('blog/posts')->push(['title' => 'Post title','body' => 'This should probably be longer.']);
		echo"<pre>";
		print_r($newPost->getvalue());
	}

	public function registerTeam(Request $request)
	{
		$team_name = $request->get('team_name');
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/sa-amazing-race-firebase-adminsdk-fesai-dcd034b635.json');
		$firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://sa-amazing-race.firebaseio.com/')->create();
		$database = $firebase->getDatabase();
		$new_team = $database->getReference('race/teams')->push([
			'team_name' => $team_name,
			'progress' => 0,
			'rank' => 0,
			'color' => '',
			'station1' => 0,
			'station2' => 0,
			'station3' => 0,
			'station4' => 0,
			'station5' => 0,
			'station6' => 0,
			'station7' => 0,
			'station8' => 0,
			'station9' => 0
		]);

		return redirect('/teamregister');
	}

	public function station($id)
	{
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/sa-amazing-race-firebase-adminsdk-fesai-dcd034b635.json');
		$firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://sa-amazing-race.firebaseio.com/')->create();
		$database = $firebase->getDatabase();
		$reference = $database->getReference('race/teams');
		$teams = $reference->getValue();

		return view('station', ['id' => $id, 'teams' => $teams]);
	}

	public function progress($id, $key)
	{
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/sa-amazing-race-firebase-adminsdk-fesai-dcd034b635.json');
		$firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://sa-amazing-race.firebaseio.com/')->create();
		$database = $firebase->getDatabase();
		$reference = $database->getReference('race/teams/');
		$teams = $reference->getValue();

		foreach($teams as $idx => $team){
			if($idx == $key){
				if($team['station'.$id] == 0){
					$progress = $team['progress']+1;
					$start_time = new \DateTime('2019-06-01T16:00:00', new \DateTimeZone('Asia/Manila'));
					$end_time = new \DateTime('NOW');
					$duration = $end_time->diff($start_time);
					$time = ($progress == 9) ? $duration->format('%h:%i') : '';
					$data = [
						'team_name' => $team['team_name'],
					    'progress' => $progress,
					    'time' => $time,
					    'rank' => 0,
					    'color'	=> $team['color'],
					    'station1' => ($id == 1) ? 1 : $team['station1'],
						'station2' => ($id == 2) ? 1 : $team['station2'],
						'station3' => ($id == 3) ? 1 : $team['station3'],
						'station4' => ($id == 4) ? 1 : $team['station4'],
						'station5' => ($id == 5) ? 1 : $team['station5'],
						'station6' => ($id == 6) ? 1 : $team['station6'],
						'station7' => ($id == 7) ? 1 : $team['station7'],
						'station8' => ($id == 8) ? 1 : $team['station8'],
						'station9' => ($id == 9) ? 1 : $team['station9']
					];
					$updates = [
					    'race/teams/'.$key => $data
					];

					$record = $database->getReference()->update($updates);

					if($record){
						return redirect("/station/$id");
					}
				}
			}
		}
	}
}
