@extends('layouts.master')

@section('css')

@stop

@section('content')
<div class="container">
	<div class="row justify-content-md-center">
		<div class="col col-lg-6">
			<h1>Amazing Race Registration</h1>
			<form method="post" action="{{ url('/teamregister') }}">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Team Name</label>
					<input type="text" class="form-control" placeholder="Team Name" name="team_name">
				</div>
				<div class="form-group">
					<label>Team Members</label>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">1</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_1">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">2</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_2">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">3</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_3">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">4</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_4">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">5</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_5">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">6</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_6">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">7</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_7">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">8</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_8">
					</div>
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">9</span>
						</div>
						<input type="text" class="form-control" placeholder="Team Member" name="team_member_9">
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Register</button>
			</form>
		</div>
	</div>
</div>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@stop