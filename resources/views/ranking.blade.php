@extends('layouts.master')

@section('css')
<style>
@font-face {
    font-family: 'Playlist';
    src: url('{{ asset("fonts/Playlist-Script.eot") }}');
    src: url('{{ asset("fonts/Playlist-Script.eot?#iefix") }}') format('embedded-opentype'),
        url('{{ asset("fonts/Playlist-Script.woff2") }}') format('woff2'),
        url('{{ asset("fonts/Playlist-Script.woff") }}') format('woff'),
        url('{{ asset("fonts/Playlist-Script.ttf") }}') format('truetype'),
        url('{{ asset("fonts/Playlist-Script.svg#Playlist-Script") }}') format('svg');
    font-weight: normal;
    font-style: normal;
}


.anniv-body:after {
    content: "";
    background: url("{{ asset('images/anniv-bg.png') }}");
    opacity: 0.3;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: -1;   
}

.sa-title {
    font-family: 'Playlist';
    margin: 20px 0;
    font-size: 3.5em;
    color: #f9a51c;
}

.sa-rank {
    font-size: 100px;
}

#clockdiv h2 {
    font-size: 100px;
}

.teams {
    display: flex;
    align-items: center;
}

.team {
    margin: 5px 0;
}

.team h1 {
    margin: 0;
}

.team i {
    color: rgb(145,37,153);
}

.progress {
    margin: 0;
}

.progress-bar {
    background-color: rgb(145,37,153);
}

.rank-gold {
    color: rgb(234, 200, 32);
}

.rank-silver {
    color: rgb(157, 185, 193);
}

.rank-bronze {
    color: rgb(204, 142, 50);
}
</style>
@stop

@section('content')
<div class="container anniv-body" ng-app="rankingApp" ng-controller="mainController">
    <div class="row justify-content-md-center">
        <div class="col text-center">
            <h1 class="sa-title">Amazing Race 2019</h1>
            <div id="clockdiv">
                <h2>
                    <span class="days"></span>:<span class="hours"></span>:<span class="minutes"></span>:<span class="seconds"></span>
                </h2>
            </div>
            <div class="row">
                <div class="col">
                    <h1 class="sa-rank rank-gold"><i class="fas fa-medal"></i></h1>
                    <h3 class="rank-gold">Team A</h3>
                </div>
                <div class="col">
                    <h1 class="sa-rank rank-silver"><i class="fas fa-medal"></i></h1>
                    <h3 class="rank-silver">Team B</h3>
                </div>
                <div class="col">
                    <h1 class="sa-rank rank-bronze"><i class="fas fa-medal"></i></h1>
                    <h3 class="rank-bronze">Team C</h3>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row teams justify-content-md-center align-items-center" ng-repeat="team in teams">
                <div class="col col-sm-1 team">
                    <h1 class="text-center"><i class="fas fa-question-circle"></i> <!-- <span class="hidden">@{{ team.team_name }}</span> --></h1>
                </div>
                <div class="col">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: @{{ (team.progress/9)*100 }}%" aria-valuenow="@{{ team.progress }}" aria-valuemin="0" aria-valuemax="9"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular.min.js"></script>
<script src="https://cdn.firebase.com/js/client/1.1.1/firebase.js"></script>
<script src="https://cdn.firebase.com/libs/angularfire/0.8.0/angularfire.min.js"></script>
<script src="{{ asset('js/rank.js') }}"></script>
<script>
function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

var deadline = 'June 1 2019 19:00:00 GMT+0800';

initializeClock('clockdiv', deadline);
</script>
@stop