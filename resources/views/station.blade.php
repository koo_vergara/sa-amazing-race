@extends('layouts.master')

@section('css')
<style>
.team {
    margin: 10px 0 10px 0;
}

.team a {
	color: #000000;
	border: 1px solid #000000;
}
</style>
@stop

@section('content')
<div class="container">
	<h2>Station {{ $id }}</h2>
	@foreach($teams as $key => $team)
	@if($team['station'.$id] == 0)
    <div class="row justify-content-md-center">
        <div class="col col-sm-4 team">
            <a class="btn btn-block" href='{{ url("/progress/$id/$key") }}' style="background: {{ $team['color'] }};">{{ $team['team_name'] }}</a>
        </div>
    </div>
    @endif
    @endforeach
</div>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@stop