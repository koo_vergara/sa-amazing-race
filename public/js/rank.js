angular.module('rankingApp', ['firebase'])
.controller('mainController', function($scope, $firebase){
	var ref = new Firebase("https://sa-amazing-race.firebaseio.com/race/teams");
	var fb = $firebase(ref);
	var syncObject = fb.$asObject();

	syncObject.$bindTo($scope, 'teams');
});